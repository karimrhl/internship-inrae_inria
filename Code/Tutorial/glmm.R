# LMM et GLMM : selection de modeles et tests

# Cas lineaire

# Modele sans effet aleatoire
# Deux effets fixes categoriels (3 x 4 modalites)

library(lme4) # version 1.1.10

n = 300 # taille d'echantillon
sigma2.eps = 4
nm.f1 = 3
nm.f2 = 4
nm.r1 = round(sqrt(n)) # effet aleatoire fictif
set.seed(0)
f1 = sample(seq(1,nm.f1), n, replace=TRUE)
f2 = sample(seq(1,nm.f2), n, replace=TRUE)
r1 = sample(seq(1,nm.r1), n, replace=TRUE)

b = c(-3)
for (i in 1:nm.f1) {
	if (i==1) {b = c(b, 0)}
      else {b = c(b, (-1)**i * i)}}

for (i in 1:nm.f2) {
	if (i==1) {b = c(b, 0)}
	else {b = c(b, (-1)**i * 2*i)}}

# Matrice de design
X.f = matrix(0, nrow=n, ncol=nm.f1+nm.f2+1)
X.f[,1] = 1
for (i in 1:nm.f1) {X.f[,i+1] = as.numeric(f1==i)}
for (i in 1:nm.f2) {X.f[,i+nm.f1+1] = as.numeric(f2==i)}

Y = X.f %*% b + sqrt(sigma2.eps) * rnorm(n)

dat.all = cbind(Y, f1, f2, r1)
dat.all = data.frame(dat.all)
names(dat.all) = c("Y", "F1", "F2", "R1")

dat.all$F1 = as.factor(dat.all$F1)
dat.all$F2 = as.factor(dat.all$F2)
dat.all$R1 = as.factor(dat.all$R1)

modf = lm(Y~ F1+F2, data=dat.all)
summary(modf)

modr = lmer(Y~ F1+F2 + (1|R1), data=dat.all)
summary(modr)

fm1P = confint(modr, method="profile", level=0.95, oldNames = FALSE)
fm1B = confint(modr, method="boot", level=0.95, oldNames = FALSE)


# Avec effet aleatoire
sigma2.r = 0.8 # avec 0.2 l'effet aleatoire devient non-significatif
Z.r = matrix(0, nrow=n, ncol=nm.r1)
for (i in 1:nm.r1) {Z.r[,i] = as.numeric(r1==i)}

set.seed(0)

U = rnorm(nm.r1, 0, 1)
# U = U/sqrt(mean((U-mean(U))^2))
U = sqrt(sigma2.r) * U

Y.r = X.f %*% b + Z.r %*% U + sqrt(sigma2.eps) * rnorm(n)

datr.all = dat.all
datr.all$Y = Y.r

modr.ran = lmer(Y~ F1+F2 + (1|R1), data=datr.all)

summary(modr.ran)

# Noter que si on estime les parametres par maximum de vraisemblance (pas par REML)
# alors R donne le BIC et AIC (necessitent la vraisemblance)
AIC(lm(Y~ F1+F2, data=datr.all))
summary(lmer(Y~ F1+F2 + (1|R1), data=datr.all, REML=FALSE))
# ... mais extractAIC permet d'obtenir AIC meme en cas d'estimation REML
extractAIC(modr.ran)

library(cAIC4)
# AIC conditionnel de Greven & Neib (2010) implemente par B. Saefken
cAIC(modr.ran)$caic

fm1P.ran = confint(modr.ran, method="profile", level=0.95, oldNames = FALSE)
fm1B.ran = confint(modr.ran, method="boot", level=0.95, oldNames = FALSE)


# Cas lineaire generalise : binomial

# Modele sans effet aleatoire
n = 500
nm.r1 = round(sqrt(n)) # effet aleatoire fictif
set.seed(0)
f1 = sample(seq(1,nm.f1), n, replace=TRUE)
f2 = sample(seq(1,nm.f2), n, replace=TRUE)
r1 = sample(seq(1,nm.r1), n, replace=TRUE)

# Matrice de design
X.f = matrix(0, nrow=n, ncol=nm.f1+nm.f2+1)
X.f[,1] = 1
for (i in 1:nm.f1) {X.f[,i+1] = as.numeric(f1==i)}
for (i in 1:nm.f2) {X.f[,i+nm.f1+1] = as.numeric(f2==i)}

Y = X.f %*% b + sqrt(sigma2.eps) * rnorm(n)

datg.all = cbind(Y, f1, f2, r1)
datg.all = data.frame(datg.all)
names(datg.all) = c("Y", "F1", "F2", "R1")

datg.all$F1 = as.factor(datg.all$F1)
datg.all$F2 = as.factor(datg.all$F2)
datg.all$R1 = as.factor(datg.all$R1)

Z.r = matrix(0, nrow=n, ncol=nm.r1)
for (i in 1:nm.r1) {Z.r[,i] = as.numeric(r1==i)}

set.seed(0)

U = rnorm(nm.r1, 0, 1)
# U = U/sqrt(mean((U-mean(U))^2))
U = sqrt(sigma2.r) * U

set.seed(1)
bp = c(-2, 0, 1, -1, 0, 3, 1, 1)
Y.g = rbinom(n, 1, exp(X.f %*% bp)/(1+exp(X.f %*% bp)))

datg.all$Y = Y.g

modpf = glm(Y~ F1+F2, family="binomial", data=datg.all)
summary(modpf)

modpr = glmer(Y~ F1+F2 + (1|R1), family="binomial", data=datg.all)
summary(modpr)
# Variance estimee a 0 exactement

fm1pP = confint(modpr, method="profile", level=0.95, oldNames = FALSE)

AIC(modpf)
extractAIC(modpr)

# Modele avec effet aleatoire

predlin.lr = X.f %*% bp + 0.1 * Z.r %*% U
# variance de l'effet aleatoire faible

predlin.hr = X.f %*% bp + Z.r %*% U
# variance de l'effet aleatoire forte

set.seed(1)
Y.lgr = rbinom(n, 1, exp(predlin.lr)/(1+exp(predlin.lr)))

datlgr.all = datg.all
datlgr.all$Y = Y.lgr

Y.hgr = rbinom(n, 1, exp(predlin.hr)/(1+exp(predlin.hr)))

dathgr.all = datg.all
dathgr.all$Y = Y.hgr

## lr
# estimation sans effet aleatoire
modblgr.f = glm(Y~ F1+F2, family="binomial", data=datlgr.all)
summary(modblgr.f)
AIC(modblgr.f)

# estimation avec effet aleatoire
modblgr.ran = glmer(Y ~ F1+F2 + (1|R1), family="binomial", data=datlgr.all)
summary(modblgr.ran)
fm1blgrP = confint(modblgr.ran, method="profile", level=0.95, oldNames = FALSE)

extractAIC(modblgr.ran)

## hr
# estimation sans effet aleatoire
modbhgr.f = glm(Y~ F1+F2, family="binomial", data=dathgr.all)
summary(modbhgr.f)

# estimation avec effet aleatoire
modbhgr.ran = glmer(Y ~ F1+F2 + (1|R1), family="binomial", data=dathgr.all)
summary(modbhgr.ran)
extractAIC(modbhgr.ran)
fm1bhgrP = confint(modbhgr.ran, method="profile", level=0.95, oldNames = FALSE)
# Effet aleatoire significatif
# cAIC(modbhgr.ran)$caic 
# absurde : 893.1841 ?

# Variable categorielle a trois modalites
n = 3000
b1 = c(-3, 0, 2, -3, 0, 4, -1, 5)
b2 = c(0, 0, -2, 3, 0, -4, 2, 5)
B = matrix(c(b1, b2), nrow=2, byrow=TRUE)

set.seed(0)
fc1 = sample(seq(1,nm.f1), n, replace=TRUE)
fc2 = sample(seq(1,nm.f2), n, replace=TRUE)
rc1 = sample(seq(1,nm.r1), n, replace=TRUE)

# Matrice de design
X.f = matrix(0, nrow=n, ncol=nm.f1+nm.f2+1)
X.f[,1] = 1
for (i in 1:nm.f1) {X.f[,i+1] = as.numeric(fc1==i)}
for (i in 1:nm.f2) {X.f[,i+nm.f1+1] = as.numeric(fc2==i)}

# Modele simule sans effet aleatoire
explinpredc = exp(X.f %*% t(B))
explinpred = explinpredc / (1+rowSums(explinpredc))
explinpred = cbind(explinpred, 1-rowSums(explinpred))

Y.c = c()
for (i in 1:n) {Y.c = c(Y.c, sample(1:3, 1, replace=TRUE, prob=explinpred[i,]))}
datcf.all = cbind(Y.c, fc1, fc2, rc1)
datcf.all = data.frame(datcf.all)
names(datcf.all) = c("Y", "F1", "F2", "R1")
for (j in 1:4) {datcf.all[,j] = as.factor(datcf.all[,j])}

# Modele estime sans effet aleatoire
library(VGAM)

BIC = function(vgamodel)
{
	
	# nombre de parametres
	d = length(attr(summary(vgamodel), "coefficients"))
	# nombre de donnees
	n = length(row.names(attr(summary(vgamodel), "x")))
	ll = attr(summary(vgamodel), "criterion")$loglikelihood
	bic = ll - 0.5 * d * log(n)
	c(d, n, ll, bic)
}

modc.f = vglm(Y ~ F1 + F2, data = datcf.all, family="multinomial")
summary(modc.f)
AIC(modc.f)

# Modele a effet aleatoire en bayesien
# Inverser les modalites 1 et 3 car MCMCglmm prend comme reference la derniere modalite
# Doit pouvoir se faire avec at.level pas bien documente, a creuser
library(MCMCglmm)
datcf.all = cbind(datcf.all, 3-as.numeric(datcf.all$Y)+1)
names(datcf.all)[5] = "Yg"
datcf.all$Yg = as.factor(datcf.all$Yg)
modcrbayes.f = MCMCglmm(Yg ~ trait + F1:trait + F2:trait, random = ~R1, rcov=~idh(trait):units,data = datcf.all, family="categorical")
# Variance nulle pour l'effet aleatoire, DIC 2792.091
summary(modcrbayes.f)
modcfbayes.f = MCMCglmm(Yg ~ trait + F1:trait + F2:trait, rcov=~idh(trait):units,data = datcf.all, family="categorical")
# Pas d'effet aleatoire, DIC 2800.553 (augmente !?)
summary(modcfbayes.f)
# On essaie le modele nul
modcfbayes.nof = MCMCglmm(Yg ~ trait, rcov=~idh(trait):units,data = datcf.all, family="categorical")
# DIC 6159.58 (augmente comme prevu)
summary(modcfbayes.nof)


# Approche "vraisemblance"
library(spaMM)

datcfspamm.all = binomialize(datcf.all,responses=c("Y"))

# Modele sans effet aleatoire
modcfspamm.f = HLfit(cbind(npos,nneg)~F1+F2,data=datcfspamm.all, family=binomial())
summary(modcfspamm.f)
# Modele avec aleatoire (ne passerait pas avec n=30000)
control.HLfit = NULL
control.HLfit$max.iter = 300
modcrspamm.f = HLfit(cbind(npos,nneg)~F1+F2 + (1|R1), control.HLfit=control.HLfit, data=datcfspamm.all, family=binomial())
summary(modcrspamm.f)
# La variance est quasiment estimee a zero

# Modele simule avec effet aleatoire
Z.r = matrix(0, nrow=n, ncol=nm.r1)
for (i in 1:nm.r1) {Z.r[,i] = as.numeric(rc1==i)}

set.seed(0)

U = rnorm(nm.r1, 0, sqrt(sigma2.r))


explinpredc.ran = exp(X.f %*% t(B) + Z.r %*% U %*% matrix(1, nrow=1, ncol=dim(B)[1]))
explinpred.ran = explinpredc.ran / (1+rowSums(explinpredc.ran))
explinpred.ran = cbind(explinpred.ran, 1-rowSums(explinpred.ran))

Y.c = c()
for (i in 1:n) {Y.c = c(Y.c, sample(1:3, 1, replace=TRUE, prob=explinpred.ran[i,]))}
datcr.all = cbind(Y.c, fc1, fc2, rc1)
datcr.all = data.frame(datcr.all)
names(datcr.all) = c("Y", "F1", "F2", "R1")
for (j in 1:4) {datcr.all[,j] = as.factor(datcr.all[,j])}

# Modele estime sans effet aleatoire

modcr.f = vglm(Y ~ F1 + F2, data = datcr.all, family="multinomial")
summary(modcr.f)
AIC(modcr.f)

# Modele a effet aleatoire en bayesien
# Inverser les modalites 1 et 3 car MCMCglmm prend comme reference la derniere modalite
datcr.all = cbind(datcr.all, 3-as.numeric(datcr.all$Y)+1)
names(datcr.all)[5] = "Yg"
datcr.all$Yg = as.factor(datcr.all$Yg)
# Pas d'effet aleatoire estime
modcfbayes.ran = MCMCglmm(Yg ~ trait + F1:trait + F2:trait, rcov=~idh(trait):units, data = datcr.all, family="categorical")
summary(modcfbayes.ran)
# DIC: 2911.438
modcrbayes.ran = MCMCglmm(Yg ~ trait + F1:trait + F2:trait, random = ~R1, rcov=~idh(trait):units, data = datcr.all, family="categorical")
# Pour ignorer les covariances entre effets fixes
# N.B. MCMCglmm(Yg ~ trait + F1:trait + F2:trait, random = ~R1, rcov=~us(trait):units,data = datcr.all, family="categorical")
# estimerait des covariances entre effets fixes 
# au prix d'une moins bonne estimation de la variance de l'effet aleatoire
summary(modcrbayes.ran)
# DIC: 2739.226

# Approche "vraisemblance"
datcrspamm.all = binomialize(datcr.all,responses=c("Y"))

# Modele sans effet aleatoire
modcfspamm.ran = HLfit(cbind(npos,nneg)~F1+F2,data=datcrspamm.all, family=binomial())
summary(modcfspamm.ran)
# Modele avec aleatoire (ne passerait pas avec n=30000)
modcrspamm.ran = HLfit(cbind(npos,nneg)~F1+F2 + (1|R1),data=datcrspamm.all, family=binomial())
summary(modcrspamm.ran)
# Un bloc par valeur de Y=k
# La variance est estimee a 0.3481 pour Y=1, 0.4331 pour Y=2. Comment contraindre l'egalite des variances ?
# On n'a pas d'intervalle de confiance ni de critere d'information.
# En tout cas on peut recuperer la vraisemblance et REML ? (voir manuel)


# Pour bien comprendre le modele dans MCMCglmm :
# Ajout d'un facteur aleatoire fictif
rc2 = sample(rc1, n, replace = FALSE)
datcr2.all = cbind(Y.c, fc1, fc2, rc1, rc2)
datcr2.all = data.frame(datcr2.all)
names(datcr2.all) = c("Y", "F1", "F2", "R1", "R2")
for (j in 1:5) {datcr2.all[,j] = as.factor(datcr2.all[,j])}


# Modele a effet aleatoire en bayesien
# Inverser les modalites 1 et 3 car MCMCglmm prend comme reference la derniere modalite
datcr2.all = cbind(datcr2.all, 3-as.numeric(datcr2.all$Y)+1)
names(datcr2.all)[5] = "Yg"
datcr2.all$Yg = as.factor(datcr2.all$Yg)
modcrbayes2.ran = MCMCglmm(Yg ~ trait + F1:trait + F2:trait, random = ~R1 + R2, rcov=~idh(trait):units, data = datcr.all, family="categorical")
